package life.maxima.jpa;

import life.maxima.jdbc.CommonDAO;
import life.maxima.jpa.entity.Post;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class PostJPADao implements AbstractDAO<Post> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(Post data) {
        em.persist(data);
    }


    @Override
    public List<Post> getAll() {
        String query = "select p from Post p";
        return em.createQuery(query, Post.class).getResultList();
    }

    @Override
    public Post getById(long id) {
        return em.find(Post.class, id);
    }

    @Override
    public void update(long id, Post data) {
        data.setPostId(id);
        em.merge(data);
    }

    @Override
    public void delete(long id) {
        em.remove(getById(id));
    }


}