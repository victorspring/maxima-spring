package life.maxima.jpa;

import life.maxima.jpa.entity.User;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
public class UserJPADao implements AbstractDAO<User>{

    @PersistenceContext
    private EntityManager em;

    @Override
    public void create(User data) {
        em.persist(data);
    }

    @Override
    public List<User> getAll() {
        String query = "select u from User u";
        return em.createQuery(query, User.class).getResultList();
    }

    @Override
    public User getById(long id) {
        return em.find(User.class, id);
    }

    @Override
    public void update(long id, User data) {
        data.setUserId(id);
        em.merge(data);
    }

    @Override
    public void delete(long id) {
        em.remove(getById(id));
    }
}