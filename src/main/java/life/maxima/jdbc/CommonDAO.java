package life.maxima.jdbc;

import life.maxima.dto.EmployeeDto;

import java.util.List;

public interface CommonDAO<T> {

    void create(T dto);

    T getById(int id);

    List<T> getAll();

    void update(T dto);

    void delete(int id);

}
