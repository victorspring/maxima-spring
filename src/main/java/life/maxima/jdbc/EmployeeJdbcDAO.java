package life.maxima.jdbc;

import life.maxima.dto.EmployeeDto;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class EmployeeJdbcDAO implements CommonDAO<EmployeeDto> {

    private static final String URL = "jdbc:postgresql://127.0.0.1:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "postgres";

    private static final String GET_ALL = "select id, first_name, last_name, salary from employee";

    @Override
    public void create(EmployeeDto dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public EmployeeDto getById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<EmployeeDto> getAll() {
        try(Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD)){
            Statement stmt =  con.createStatement();
            ResultSet result = stmt.executeQuery(GET_ALL);

            List<EmployeeDto> list = new ArrayList<>();
            while (result.next()){
                int id = result.getInt("id");
                String firstName = result.getString("first_name");
                String lastName = result.getString("last_name");
                int salary = result.getInt("salary");

                list.add(new EmployeeDto(id, firstName, lastName, salary));
            }

            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public void update(EmployeeDto dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }
}
