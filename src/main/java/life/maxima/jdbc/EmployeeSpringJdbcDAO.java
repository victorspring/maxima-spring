package life.maxima.jdbc;

import life.maxima.dto.EmployeeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeSpringJdbcDAO implements CommonDAO<EmployeeDto> {

    private static final String GET_ALL = "select id, first_name, last_name, salary from employee";
    private static final String GET_BY_ID = """
        select
            id, first_name, last_name, salary
        from
            employee
        where
            id = ?
    """;
    private static final String CREATE = "insert into employee(first_name, last_name, salary)\n" +
            "values (?, ?, ?)";

    private static final String UPDATE = """
        update employee
        set
            first_name = ?,
            last_name = ?,
            salary = ?
        where
            id = ?
    """;

    private static final String DELETE = "delete from employee where id = ?";

    private static final RowMapper<EmployeeDto> mapper = (result, rowNum) -> {
        int id = result.getInt("id");
        String firstName = result.getString("first_name");
        String lastName = result.getString("last_name");
        int salary = result.getInt("salary");
        return new EmployeeDto(id, firstName, lastName, salary);
    };

    private final JdbcOperations jdbcOperations;

    @Autowired
    public EmployeeSpringJdbcDAO(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public void create(EmployeeDto dto) {
        jdbcOperations.update(CREATE,
                dto.getFirstName(),
                dto.getLastName(),
                dto.getSalary());
    }

    @Override
    public EmployeeDto getById(int id) {
        return jdbcOperations.queryForObject(GET_BY_ID, mapper, id);
    }

    @Override
    public List<EmployeeDto> getAll() {
        return jdbcOperations.query(GET_ALL, mapper);
    }

    @Override
    public void update(EmployeeDto dto) {
        jdbcOperations.update(UPDATE,
                dto.getFirstName(),
                dto.getLastName(),
                dto.getSalary(),
                dto.getId());
    }

    @Override
    public void delete(int id) {
        jdbcOperations.update(DELETE, id);
    }
}
