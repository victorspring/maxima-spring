package life.maxima.jdbc;
import life.maxima.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class ProductSpringJdbcDAO implements CommonDAO<ProductDto> {

    private static final String GET_ALL = "select id, description, cost, count from product";
    private static final String GET_BY_ID = """
        select
            id, description, cost, count
        from
            product
        where
            id = ?
    """;
    private static final String CREATE = "insert into product(description, cost, count)\n" +
            "values (?, ?, ?)";

    private static final String UPDATE = """
        update product
        set
            description = ?,
            cost = ?,
            count = ?
        where
            id = ?
    """;
    private static final String DELETE = "delete from product where id = ?";

    private static final RowMapper<ProductDto> mapper = (result, rowNum) -> {
        int id = result.getInt("id");
        String description = result.getString("description");
        int count = result.getInt("count");
        BigDecimal cost = result.getBigDecimal("cost");

        return new ProductDto(id, description, cost, count);
    };

    private final JdbcOperations jdbcOperations;

    @Autowired
    public ProductSpringJdbcDAO(JdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public void create(ProductDto dto) {
        jdbcOperations.update(CREATE,
                dto.getDescription(),
                dto.getCost(),
                dto.getCount());
    }

    @Override
    public ProductDto getById(int id) {
        return jdbcOperations.queryForObject(GET_BY_ID,mapper,id);
    }

    @Override
    public List<ProductDto> getAll() {
        return jdbcOperations.query(GET_ALL, mapper);
    }

    @Override
    public void update(ProductDto dto) {
        jdbcOperations.update(UPDATE,
                dto.getDescription(),
                dto.getCost(),
                dto.getCount(),
                dto.getId());
    }

    @Override
    public void delete(int id) {
        jdbcOperations.update(DELETE, id);
    }
}

