package life.maxima.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class ProductDto {

    private long id;
    private String description;
    private BigDecimal cost;
    private int count;



}
