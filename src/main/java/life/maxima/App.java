package life.maxima;

import life.maxima.dto.EmployeeDto;
import life.maxima.jdbc.CommonDAO;
import life.maxima.spring.config.DBConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(DBConfig.class);

        CommonDAO<EmployeeDto> employeeDao =
//                (CommonDAO<EmployeeDto>)context.getBean("employeeJdbcDAO");
                (CommonDAO<EmployeeDto>)context.getBean("employeeSpringJdbcDAO");

        System.out.println("Get All");
        employeeDao.getAll().forEach(System.out::println);
        System.out.println();

        System.out.println("Create");
        employeeDao.create(new EmployeeDto("Elon", "Musk", 1000));
        System.out.println();

        System.out.println("Get All");
        employeeDao.getAll().forEach(System.out::println);
        System.out.println();

        System.out.println("Update");
        employeeDao.update(new EmployeeDto(6, "Elona", "Musk", 1000000));
        System.out.println();

        System.out.println("Get By Id");
        System.out.println(employeeDao.getById(6));
        System.out.println();

        System.out.println("Delete");
        employeeDao.delete(6);
        System.out.println();

        System.out.println("Get All");
        employeeDao.getAll().forEach(System.out::println);
        System.out.println();

    }
}
