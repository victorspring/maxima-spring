package life.maxima.spring.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component("myComputer")
public class Computer {

    private CPU cpu;
    private Screen screen;
    private List<RAM> ramList;
    private List<Storage> storageList;



    @Autowired
    public Computer(@Qualifier("intelCPU") CPU cpu,
                    @Qualifier("myScreen1") Screen screen,
                    @Qualifier("ramList") List<RAM> ramList,
                    List<Storage> storageList) {
        this.cpu = cpu;
        this.screen = screen;
        this.ramList = ramList;
        this.storageList = storageList;
    }



}
