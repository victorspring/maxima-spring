package life.maxima.spring.bean;

public interface Device {

    default String getName(){
        return this.getClass().getSimpleName();
    }
}
