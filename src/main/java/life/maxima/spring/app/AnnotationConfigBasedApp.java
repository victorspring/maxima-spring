package life.maxima.spring.app;

import life.maxima.spring.bean.Computer;
import life.maxima.spring.bean.Device;
import life.maxima.spring.config.AnnotationConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;
import java.util.stream.Collectors;

public class AnnotationConfigBasedApp {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(AnnotationConfig.class);
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));

        Computer computer = (Computer)context.getBean("myComputer");
        System.out.println("CPU: " + computer.getCpu().getName());
        System.out.println("RAM: " + computer.getRamList()
                .stream()
                .map(Device::getName)
                .collect(Collectors.joining(", ")));
        System.out.println("Screen: " + computer.getScreen().getName());
        System.out.println("Storage: " + computer.getStorageList().stream()
                .map(Device::getName)
                .collect(Collectors.joining(", ")));

        Computer anotherComputer = (Computer)context.getBean("myComputer");

        System.out.println(computer == anotherComputer);


    }

}
