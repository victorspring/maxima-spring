package life.maxima.spring.config;

import life.maxima.spring.bean.HyperXRAM;
import life.maxima.spring.bean.KingstonRAM;
import life.maxima.spring.bean.RAM;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.List;

@Configuration
@ComponentScan("life.maxima.spring.bean")
public class AnnotationConfig {

    @Bean
    public List<RAM> ramList(){
        return List.of(new KingstonRAM(), new HyperXRAM(), new KingstonRAM());
    }

}
