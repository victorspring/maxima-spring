package life.maxima.spring.config;

import life.maxima.spring.bean.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.List;

@Configuration
public class JavaConfig {

    @Bean("myComputer")
    public Computer computer(){
        Computer computer = new Computer(
                intel(),
                dell(),
                List.of(kingston(), hyperx(), kingston()),
                List.of(ssd(), hdd(), hdd())
        );
        return computer;
    }

    @Bean
    public CPU intel(){
        return new IntelCPU();
    }

    @Bean
    public CPU amd(){
        return new AmdCPU();
    }

    @Bean
    public Screen dell(){
        return new DellScreen();
    }

    @Bean
    public Screen phillips(){
        return new PhillipsScreen();
    }

    @Bean
    @Scope("prototype")
    public RAM kingston(){
        return new KingstonRAM();
    }

    @Bean
    @Scope("prototype")
    public RAM hyperx(){
        return new HyperXRAM();
    }

    @Bean
    @Scope("prototype")
    public Storage ssd(){
        return new SSDStorage();
    }

    @Bean
    @Scope("prototype")
    public Storage hdd(){
        return new HDDStorage();
    }


}
