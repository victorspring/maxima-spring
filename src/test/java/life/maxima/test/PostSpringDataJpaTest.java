package life.maxima.test;

import life.maxima.jpa.entity.Post;
import life.maxima.jpa.repository.PostRepository;
import life.maxima.jpa.repository.UserRepository;
import life.maxima.spring.config.DBConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DBConfig.class)
@Sql(scripts = "classpath:blog.sql",
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
public class PostSpringDataJpaTest {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Autowired
    public PostSpringDataJpaTest(PostRepository postRepository,
                                 UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }


    @Test
    void create(){
        Post post = new Post();
        post.setUser(userRepository.findById(2L).orElseThrow());
        post.setTitle("Day 4");
        post.setContent("All is ok again");
        post.setDtCreated(LocalDateTime.now());

        postRepository.save(post);

        assertEquals("Day 4", postRepository.findById(4L).orElseThrow().getTitle());
    }


    @Test
    void update(){
        Post post = postRepository.findById(1L).orElseThrow();
        post.setTitle("Day 4");
        post.setDtUpdated(LocalDateTime.now());

        postRepository.save(post);
        assertEquals("Day 4", postRepository.findById(1L).orElseThrow().getTitle());
        assertNotNull(postRepository.findById(1L).orElseThrow().getDtUpdated());
    }

    @Test
    void delete() {
        postRepository.deleteById(1L);
        assertEquals(2, postRepository.findAll().size());
    }

    @Test
    void postTagComment(){
        Post post = postRepository.findById(1L).orElseThrow();
        assertEquals(3, post.getComments().size());
        assertEquals(2, post.getTags().size());
    }

    @Test
    void findByTitle(){
        Post post = postRepository.findByTitle("Day 1").get(0);
        assertEquals("It's all good!", post.getContent());
    }

    @Test
    void findByDtCreatedBetween(){
        List<Post> posts = postRepository.findByDtCreatedBetween(
                LocalDateTime.now().minusDays(3), LocalDateTime.now().minusDays(2));

        assertEquals(1, posts.size());

    }



}
