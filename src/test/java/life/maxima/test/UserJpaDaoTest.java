package life.maxima.test;

import life.maxima.jpa.AbstractDAO;
import life.maxima.jpa.entity.User;
import life.maxima.spring.config.DBConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DBConfig.class)
@Sql(scripts = "classpath:blog.sql",
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Transactional
public class UserJpaDaoTest {

    private final AbstractDAO<User> postDao;

    @Autowired
    public UserJpaDaoTest(AbstractDAO<User> postDao) {
        this.postDao = postDao;
    }

    @Test
    void create(){
        User user = new User();
        user.setUsername("user2");
        user.setPassword("user2");
        user.setDtCreated(LocalDateTime.now());

        postDao.create(user);

        assertEquals("user2", postDao.getById(3).getUsername());
    }


    @Test
    void update(){
        User user = new User();
        user = postDao.getById(2);
        user.setUsername("updated_user");
        user.setDtCreated(LocalDateTime.now());

        postDao.update(2, user);

        user = postDao.getById(2);
        assertEquals("updated_user", user.getUsername());
        assertNotNull(user.getDtCreated());
    }

    @Test
    void delete() {
        postDao.delete(1);
        assertEquals(1, postDao.getAll().size());
    }

    @Test
    void userComments(){
        User user = postDao.getById(1);
        assertEquals(3, user.getComments().size());
    }



}
