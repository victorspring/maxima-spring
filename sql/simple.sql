create table numbers(
	val integer
);
insert into numbers(val) values (1);
insert into numbers(val) values (2);
insert into numbers(val) values (9);


select val number,val+1 "incremented value" from numbers;

-- 1) Many To One

-- DDL
drop table if exists employee;
drop table if exists department;

create table department(
	id serial primary key,
	name varchar(50)
);

create table employee(
	id serial primary key,
	first_name varchar(50),
	last_name varchar(50),
	department_id int references department(id),
	salary int
);


-- DML
insert into department(name)
    values ('IT');
insert into department(name)
    values ('Management');
insert into department(name)
    values ('QA');

insert into employee(first_name, last_name, salary, department_id)
	values ('Victor', 'Bodrov', 10000000, 1);
insert into employee(first_name, last_name, salary, department_id)
	values ('Ivan', 'Ivanov', 5000000, 1);
insert into employee(first_name, last_name, salary, department_id)
	values ('Ivan', 'Petrov', 999000, 2);
insert into employee(first_name, last_name, salary, department_id)
	values ('Masha', 'Ivanova', 6000000, 2);
insert into employee(first_name, last_name, salary, department_id)
	values ('Dasha', 'Petrova', 16000000, null);

-- Query

select * from department;

-- DISTINCT
select distinct first_name from employee;

-- ORDER BY
select * from employee order by id desc;

-- FILTRATION
select * from employee
    where salary >= 5000000 and department_id = 1;

-- FUNCTIONS
select upper(first_name), lower(last_name) from employee;


-- AGGREGATE FUNCTIONS
select count(*) from employee;

select avg(salary) from employee
   where department_id = 1;

select avg(salary) from employee
   group by department_id;

-- CROSS JOIN WITH FILTRATION
select
    *
from
    employee e, department d
where
    e.department_id = d.id;

-- INNER JOIN
select
    *
from
    employee e
       inner join
    department d
        on e.department_id = d.id;

-- LEFT JOIN
select
    *
from
    employee e
       left join
    department d
        on e.department_id = d.id;

-- RIGHT JOIN
select
    *
from
    employee e
       right join
    department d
        on e.department_id = d.id;


-- 1) Many To Many

-- DDL
drop table if exists employee_department;
drop table if exists employee;
drop table if exists department;


create table employee(
	id serial primary key,
	first_name varchar(50),
	last_name varchar(50),
	salary int
);

create table department(
	id serial primary key,
	name varchar(50)
);

create table employee_department(
    employee_id int references employee(id),
    department_id int references department(id),
    primary key (employee_id, department_id)
);

-- DML
insert into department(name)
    values ('IT');
insert into department(name)
    values ('Management');
insert into department(name)
    values ('QA');

insert into employee(first_name, last_name, salary)
	values ('Victor', 'Bodrov', 10000000);
insert into employee(first_name, last_name, salary)
	values ('Ivan', 'Ivanov', 5000000);
insert into employee(first_name, last_name, salary)
	values ('Ivan', 'Petrov', 999000);
insert into employee(first_name, last_name, salary)
	values ('Masha', 'Ivanova', 6000000);
insert into employee(first_name, last_name, salary)
	values ('Dasha', 'Petrova', 16000000);

insert into employee_department values (1, 1);
insert into employee_department values (1, 2);
insert into employee_department values (2, 1);
insert into employee_department values (3, 2);
insert into employee_department values (4, 1);
insert into employee_department values (4, 2);

-- Query

select
    *
from
    employee e
        inner join employee_department ed
            on e.id = ed.employee_id
        inner join department d
            on d.id = ed.department_id;

select
    e.first_name,
    e.last_name,
    e.salary
from
    employee e
        inner join employee_department ed
            on e.id = ed.employee_id
        inner join department d
            on d.id = ed.department_id
    where
        e.salary >= 5000000
        and
        d.name = 'IT';
