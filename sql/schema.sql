-- DDL

drop table if exists order_product;
drop table if exists product;
drop table if exists "order";
drop table if exists customer;


create table customer (
    id bigserial primary key,
    first_last_name varchar(100)
);

create table "order" (
    id bigserial primary key,
    customer_id int references customer(id),
    dt_created timestamp
);

create table product (
    id bigserial primary key,
    description text unique,
    cost numeric(10, 2),
    count int
);

create table order_product (
    order_id int references "order"(id),
    product_id int references product(id),
    count int,
    primary key (order_id, product_id)
);

-- DML

insert into product(description, cost, count)
    values ('Laptop', 1000, 5);
insert into product(description, cost, count)
    values ('Headphones', 10, 10);
insert into product(description, cost, count)
    values ('Phone', 100, 1);

insert into customer(first_last_name)
    values ('Ivan Ivanov');
insert into customer(first_last_name)
    values ('Masha Ivanova');

insert into "order"(customer_id, dt_created)
    values (1, now());
insert into "order"(customer_id, dt_created)
    values (1, now());
insert into "order"(customer_id, dt_created)
    values (2, now());

insert into order_product(order_id, product_id, count)
    values (1, 1, 1);
insert into order_product(order_id, product_id, count)
    values (1, 2, 2);
insert into order_product(order_id, product_id, count)
    values (2, 3, 2);
insert into order_product(order_id, product_id, count)
    values (3, 2, 1);
insert into order_product(order_id, product_id, count)
    values (3, 3, 1);


select
    c.first_last_name "First Last Name",
    p.description "Description"
from
    customer c
        inner join "order" o
            on o.customer_id = c.id and c.first_last_name like '%Ivanov%'
        inner join order_product op
            on op.order_id = o.id
        inner join product p
            on op.product_id = p.id;





select * from product;
select * from customer;


--USER, ROLE, POST, COMMENT, TAG